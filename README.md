Curtain Raiser module:
------------------------
Requires - Drupal 8

Overview:
---------
Curtain raiser provides a curtain overlay for the webiste with an inaugurate button on click the curtain open with an animation effect.

Usage :
-------
Install the module and set password for inauguration from the curtain raiser settings. The curtain overlay will be shown only if you have set the password.

Configurations:
---------------
Administration > Configuration > Development > Curtain Raiser Settings.

Features:
---------
1. Password protection for the inauguration.
2. Curtain overlay based on the inauguration status.
